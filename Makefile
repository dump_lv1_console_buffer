
CC=gcc
CFLAGS=-Wall -O2 -g
LDFLAGS=

SRC=dump_lv1_console_buffer.c
OBJ=$(SRC:.c=.o)
TARGET=dump_lv1_console_buffer

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) $(LDFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c $<

.PHONY: clean
clean:
	rm -f $(OBJ) $(TARGET)
