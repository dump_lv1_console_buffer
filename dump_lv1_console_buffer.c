
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define VERSION				"0.0.1"

struct opts {
	char *device_name;
	unsigned int lv1_version;
	int do_help;
	int do_verbose;
	int do_version;
};

struct lv1_console_offset {
	unsigned int lv1_version;
	unsigned int start_offset;
};

static struct option long_opts[] = {
	{ "help",	no_argument, NULL, 'h' },
	{ "verbose",	no_argument, NULL, 'v' },
	{ "version",	no_argument, NULL, 'V' },
	{ NULL, 0, NULL, 0 }
};

static struct lv1_console_offset lv1_console_offsets[] = {
	{ 315, 0x0034fd00 },
	{ 341, 0x0034d970 },
	{ 355, 0x00350b90 },
};

/*
 * usage
 */
static void usage(void) {
	fprintf(stderr,
		"Usage: dump_lv1_console_buffer [OPTIONS] DEVICE LV1VERSION\n"
		"\n"
		"Options:\n"
		"	-h, --help		Show this message and exit\n"
		"	-v, --verbose		Increase verbosity\n"
		"	-V, --version		Show version information and exit\n"
		"\n\n"
		"Simple example:\n"
		"	dump_lv1_console_buffer /dev/ps3ram 355\n");
}

/*
 * version
 */
static void version(void)
{
	fprintf(stderr,
		"dump_lv1_console_buffer " VERSION "\n"
		"Copyright (C) 2011 graf_chokolo <grafchokolo@googlemail.com>\n"
		"This is free software.  You may redistribute copies of it "
		"under the terms of\n"
		"the GNU General Public License 2 "
		"<http://www.gnu.org/licenses/gpl2.html>.\n"
		"There is NO WARRANTY, to the extent permitted by law.\n");
}

/*
 * process_opts
 */
static int process_opts(int argc, char **argv, struct opts *opts)
{
#define N(a)	(sizeof(a) / sizeof(a[0]))

	int c, i;
	char *endptr;

	while ((c = getopt_long(argc, argv, "hvV", long_opts, NULL)) != -1) {
		switch (c) {
		case 'h':
		case '?':
			opts->do_help = 1;
			return 0;

		case 'v':
			opts->do_verbose++;
			break;

		case 'V':
			opts->do_version = 1;
			return 0;

		default:
			fprintf(stderr, "Invalid command option: %c\n", c);
			return -1;
		}
	}

	if (optind >= argc) {
		fprintf(stderr, "No device specified\n");
		return -1;
	}

	opts->device_name = argv[optind];
	optind++;

	if (optind >= argc) {
		fprintf(stderr, "No lv1 version specified\n");
		return -1;
	}

	opts->lv1_version = strtoul(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid lv1 version specified: %s\n", argv[optind]);
		return -1;
	}

	for (i = 0; i < N(lv1_console_offsets); i++) {
		if (lv1_console_offsets[i].lv1_version == opts->lv1_version)
			break;
	}

	if (i >= N(lv1_console_offsets)) {
		fprintf(stderr, "Unknown lv1 version specified: %s\n", argv[optind]);
		return -1;
	}

	optind++;

	return 0;

#undef N
}

/*
 * get_lv1_console_buffer
 *
 * No, it's not magic, to understand what i'm doing here
 * see my reverse engineered PS3 hypervisor dump :-;
 * 
 * HV rules !!!
 */
static int get_lv1_console_buffer(int fd, unsigned int start_offset,
	off_t *buf_offset, unsigned int *buf_size)
{
	uint64_t offset, size_offset, size;
	int error;

	error = lseek(fd, start_offset, SEEK_SET);
	if (error < 0)
		return error;

	error = read(fd, &offset, sizeof(offset));
	if (error != sizeof(offset))
		return error;

	error = lseek(fd, offset, SEEK_SET);
	if (error < 0)
		return error;

	error = read(fd, &offset, sizeof(offset));
	if (error != sizeof(offset))
		return error;

	error = lseek(fd, offset, SEEK_SET);
	if (error < 0)
		return error;

	error = read(fd, &offset, sizeof(offset));
	if (error != sizeof(offset))
		return error;

	error = lseek(fd, offset + 0x60, SEEK_SET);
	if (error < 0)
		return error;

	error = read(fd, &offset, sizeof(offset));
	if (error != sizeof(offset))
		return error;

	error = lseek(fd, offset, SEEK_SET);
	if (error < 0)
		return error;

	error = read(fd, &size_offset, sizeof(size_offset));
	if (error != sizeof(size_offset))
		return error;

	error = lseek(fd, size_offset, SEEK_SET);
	if (error < 0)
		return error;

	error = read(fd, &size, sizeof(size));
	if (error != sizeof(size))
		return error;

	error = lseek(fd, offset + 0x20, SEEK_SET);
	if (error < 0)
		return error;

	error = read(fd, &offset, sizeof(offset));
	if (error != sizeof(offset))
		return error;

	*buf_offset = offset;
	*buf_size = size;

	return 0;
}

/*
 * main
 */
int main(int argc, char **argv)
{
#define N(a)	(sizeof(a) / sizeof(a[0]))

	struct opts opts;
	int fd = 0, error = 0, i;
	off_t buf_offset = 0;
	unsigned int buf_size = 0;
	char c;

	memset(&opts, 0, sizeof(opts));

	if (process_opts(argc, argv, &opts)) {
		usage();
		error = 1;
		goto done;
	}

	if (opts.do_help) {
		usage();
		goto done;
	} else if (opts.do_version) {
		version();
		goto done;
	}

	fd = open(opts.device_name, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "%s: %s\n", opts.device_name, strerror(errno));
		error = 2;
		goto done;
	}

	for (i = 0; i < N(lv1_console_offsets); i++) {
		if (lv1_console_offsets[i].lv1_version == opts.lv1_version)
			break;
	}

	error = get_lv1_console_buffer(fd, lv1_console_offsets[i].start_offset,
		&buf_offset, &buf_size);
	if (error) {
		fprintf(stderr, "%s: %s\n", opts.device_name, strerror(errno));
		error = 3;
		goto done;
	}

	if (opts.do_verbose)
		fprintf(stdout, "lv1 console buffer offset 0x%08lx size %d\n",
			buf_offset, buf_size);

	error = lseek(fd, buf_offset, SEEK_SET);
	if (error < 0) {
		fprintf(stderr, "%s: %s\n", opts.device_name, strerror(errno));
		error = 3;
		goto done;
	}

	for (i = 0; i < buf_size; i++) {
		error = read(fd, &c, 1);
		if (error != 1) {
			fprintf(stderr, "%s: %s\n", opts.device_name, strerror(errno));
			error = 3;
			goto done;
		}

		fprintf(stdout, "%c", c);
	}

done:

	if (fd >= 0)
		close(fd);

	exit(error);

#undef N
}
